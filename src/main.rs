extern crate sysfs_gpio;
extern crate pn532;

use std::io::{stdout, Write};
use std::thread;
use std::time::Duration;
use sysfs_gpio::{Pin, Direction, Edge};
use pn532::{Pn532, I2CResult, MifareKeyTypes, responses::Response};
use ndef_parser::well_known::text::Text;

const RESET_PIN: u64 = 20;
const REQ_PIN: u64 = 16;
const PN532_I2C_ADDRESS: u8 = 0x48 >> 1;
const PN532_I2C_READBIT: u8 = 0x01;
const PN532_I2C_BUSY: u8 = 0x00;
const PN532_I2C_READY: u8 = 0x01;
const PN532_I2C_READYTIMEOUT: u8 = 20;

// const NFC532_SLAVE_ADDR: u16 = 0x48 >> 1;
// const REGISTER_ADDR: u8 = 0x24;

fn interrupt(pin: u64) -> sysfs_gpio::Result<()> {
    let input = Pin::new(pin);
    input.with_exported(|| {
        input.set_direction(Direction::In)?;
        input.set_edge(Edge::RisingEdge)?;
        let mut poller = input.get_poller()?;

        let mut total: u32 = 0;

        loop {
            match poller.poll(1000)? {
                Some(value) => {
                    total += value as u32;
                    println!("{}", total);
                },
                None => {
                    let mut stdout = stdout();
                    stdout.write_all(b".")?;
                    stdout.flush()?;
                }
            }
        }
    })
}

fn main() -> I2CResult<()> {
    let mut card_reader = Pn532::open("/dev/i2c-1").unwrap();
    let fwv = card_reader.get_firmware_version()?;
    let timeout = Duration::from_secs(5);
    println!("nfc firmware version: {}.{}", fwv.1, fwv.2);

    card_reader.setup()?;

    println!("Waiting for card...");

    let mut clefs_opt: Option<Response> = None;
    loop {
        let result = card_reader.list2(timeout);

        match result {
            Ok(clefs) => {
                clefs_opt.replace(clefs);
                break;
            },
            Err(_e) => {
                println!("No card detected...");
                ()
            }
        }
    }

    thread::sleep(Duration::from_millis(10));

    let data = clefs_opt.unwrap().data.unwrap();

    let targets = data.targets;
    let target = targets.first().unwrap();

    let default_key = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF];

    thread::sleep(Duration::from_millis(10));

    let mut content = vec![];
    for block in 4..7 {
        card_reader.mifare_classic_authenticate_block(&target.nfcid, block, MifareKeyTypes::MifareAuthB, &default_key)?;

        let block_content = card_reader.mifare_classic_read_block(block)?;

        content.extend_from_slice(&block_content[2..]);
    }

    let ndef = ndef_parser::ndef::NDEF::parse(&content).unwrap();

    let text = Text::parse(&ndef).unwrap();

    println!("fuck yeah: {:?}", text);

    Ok(())
}

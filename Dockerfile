FROM rust:1.62.0

RUN rustup target add armv7-unknown-linux-gnueabihf

RUN apt-get update && \
    apt-get install -y build-essential gcc-arm-linux-gnueabihf

#!/usr/bin/env bash

set -e

#USER="lunfel"
RASPBERRYPI="192.168.181.210"
TARGET="armv7-unknown-linux-gnueabihf"

#docker build -t ubuntu:raspberry .
#docker run -it --rm -v $(pwd):/project rust:1.62.0 cargo build

# CARGO_TARGET_ARMV7_UNKNOWN_LINUX_GNUEABIHF_LINKER: https://github.com/rust-lang/rust/issues/28924#issuecomment-580828030
docker run --rm --user "$(id -u)":"$(id -g)" -e CARGO_HOME=/usr/src/alfred/cargo_home -e CARGO_TARGET_ARMV7_UNKNOWN_LINUX_GNUEABIHF_LINKER=/usr/bin/arm-linux-gnueabihf-gcc -v "$PWD":/usr/src/alfred -w /usr/src/alfred registry.gitlab.com/lunfel/alfred/rust-compiler cargo build --verbose --target=$TARGET

#cargo build --offline

#scp target/$TARGET/debug/alfred alfred@$RASPBERRYPI:~
#
#ssh alfred@$RASPBERRYPI 'sudo ./alfred'
